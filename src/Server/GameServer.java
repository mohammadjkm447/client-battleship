package Server;

import Server.Controler.GameTread;
import Server.Network.ClientTread;
import Server.Network.GameLobby;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class GameServer {

    //initialize socket and input stream
    private Socket socket= null;
    private ServerSocket server= null;
    private DataInputStream in=  null;

    // constructor with port
    public GameServer(int port) {
        try {
            server = new ServerSocket(port);
            System.out.println("Server started");
            GameLobby gameLobby =new GameLobby();
            System.out.println("Make game lobby");

            System.out.println("Waiting for a client ...");

            while (true){
                socket = server.accept();
//                gameLobby.AddClient(socket);
                System.out.println("Client accepted");
//                if(gameLobby.getWait().size()==2){
//                    GameTread gameTread = new GameTread();
//                }

                ClientTread clientTread =new ClientTread(socket);
                clientTread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        GameServer server = new GameServer(447);
    }
}
