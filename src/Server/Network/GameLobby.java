package Server.Network;

import java.net.Socket;
import java.util.ArrayList;

public class GameLobby {
    private ArrayList<Socket> wait;

    public GameLobby() {
        wait=new ArrayList<>();
    }


    public void AddClient(Socket socket){
        wait.add(socket);
    }

    public ArrayList<Socket> getWait() {
        return wait;
    }

    public void setWait(ArrayList<Socket> wait) {
        this.wait = wait;
    }
}
