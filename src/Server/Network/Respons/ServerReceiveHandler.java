package Server.Network.Respons;

import battleship.client.network.response.*;
import com.google.gson.Gson;

public class ServerReceiveHandler {

    String input;

    public ServerReceiveHandler(String str){
        input = str;
        new Thread(this::run).start();
    }

    private Response getResponseClass(String type, String data, Gson gson){
        Response response = null;
        if(type.equals(ConnectedResponse.tag)){
            response = gson.fromJson(data, ConnectedResponse.class);
        }
        else if(type.equals(GameAcceptedResponse.tag)){
            response = gson.fromJson(data, GameAcceptedResponse.class);
        }
        else if(type.equals(GameEndedResponse.tag)){
            response = gson.fromJson(data, GameEndedResponse.class);
        }
        else if(type.equals(SetFiredResponse.tag)){
            response = gson.fromJson(data, SetFiredResponse.class);
        }
        //todo write other if



        return response;
    }

    private void run(){
        Gson gson = new Gson();
        String[] str = gson.fromJson(input, String[].class);
        for(int i = 0; i < str.length; i++){
            String type = str[i];
            String data = str[i+1];
            Response response = getResponseClass(type, data, gson);
            response.process();
        }
    }
}
