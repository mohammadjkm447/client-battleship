package Server.Network;

import Server.Network.Respons.ServerReceiveHandler;
import battleship.client.Log;
import battleship.client.network.AppSocket;
import battleship.client.network.Receiver;
import battleship.client.network.response.ReceiveHandler;

import java.io.IOException;

public class ServerReceiver {
    public static final String tag = "SERVER_RECEIVER";
    private Thread thread;
    private static ServerReceiver receiver;
    public static ServerReceiver getInstance() {
        return receiver;
    }
    public ServerReceiver(){
        receiver = this;
        thread = new Thread(this::run);
        thread.start();
    }

    public void interrupt(){
        thread.interrupt();
    }

    void run(){
        while (true){
            if(AppSocket.getInstance().getIsConnected()){
                try {
                    String input = AppSocket.getInstance().receive();
                    Log.log(tag, "received: "+input);
                    new ServerReceiveHandler(input);
                } catch (IOException e) {
                    Log.log(tag, "exception :"+e.getMessage());
                }
            }
            else {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {}
            }
            if(Thread.interrupted()){
                return;
            }
        }
    }
}
