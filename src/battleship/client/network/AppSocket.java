package battleship.client.network;

import battleship.client.Controller.PageController;
import battleship.client.network.Request.FirstConnectionRequest;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

public class AppSocket{
    public static final String tag = "SOCKET";
    private static AppSocket appSocket;
    public static AppSocket getInstance() {
        return appSocket;
    }

    private Socket socket;
    private InputStream in;
    private OutputStream out;
    private String ip = "localhost";
    private int port = 3333;
    private String username;
    private String password;
    private boolean createAccount;
    private ConnectionChecker connectionChecker;
    private boolean isConnected;

    public AppSocket(String username, String password, boolean createAccount){
        appSocket = this;
        this.username = username;
        this.password = password;
        this.createAccount = createAccount;
        new Thread(this::firstConnect).start();
    }

    private void firstConnect(){
        try {
            socket = new Socket(this.ip, this.port);
            in = socket.getInputStream();
            out = socket.getOutputStream();
            FirstConnectionRequest request = new FirstConnectionRequest(username, password, createAccount);
            Sender.sent(request.getString());
        } catch (IOException e) {
            String error = "Cannot Connect";
            System.out.println(error);
            if(createAccount){
                PageController.getInstance().setCreateError(error);
            }
            else{
                PageController.getInstance().setLoginError(error);
            }
        }
    }

    void connect(){
        while (true){
            try {
                socket = new Socket(ip, port);
                break;
            } catch (IOException e) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

        }
        //todo make request
    }

    //todo make a lock for receive method

    public void setIsConnected(boolean isConnected) {
        this.isConnected = isConnected;
        if(isConnected){
            connectionChecker = new ConnectionChecker();
            new Receiver();
        }else {
            Receiver.getInstance().interrupt();
            //todo make another connection
        }
    }

    public boolean getIsConnected(){
        return isConnected;
    }

    public void send (ByteBuffer buffer) throws IOException {
        out.write(buffer.array());
        out.flush();
    }

    public String receive () throws IOException {
        while (in.read() != 1);
        byte[] byteLength = new byte[4];
        in.read(byteLength);
        int length = ByteBuffer.wrap(byteLength).getInt();
        byte[] byteStr = new byte[length];
        in.read(byteStr);
        String str = new String(byteStr);
        return str;
    }

}
