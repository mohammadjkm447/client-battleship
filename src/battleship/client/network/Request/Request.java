package battleship.client.network.Request;

import battleship.client.Log;
import battleship.client.network.AppSocket;
import battleship.client.network.Sender;
import com.google.gson.Gson;

import java.util.Arrays;

public abstract class Request{
    private static final String tag = "Request";

    public String getString(String reqType){
        Gson gson =  new Gson();
        String str = gson.toJson(this);
        return gson.toJson(Arrays.asList(reqType, str));
    }
    abstract String getString();

    public void process(){
        new Thread(this::run).start();
    }
    void run(){
        String str = getString();
        Sender.sent(str);
    }

}
