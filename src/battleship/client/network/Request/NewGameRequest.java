package battleship.client.network.Request;

public class NewGameRequest extends Request{
    public static final String tag = "NEW_GAME_REQUEST";


    @Override
    public String getString() {
        return getString(tag);
    }
}
