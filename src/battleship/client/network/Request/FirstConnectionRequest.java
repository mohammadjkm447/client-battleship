package battleship.client.network.Request;

import battleship.client.Log;

public class FirstConnectionRequest extends Request {
    public static final String tag = "FIRST_CONNECTION_REQUEST";

    String username;
    String password;
    boolean createAccount;

    public FirstConnectionRequest(String username, String password, boolean createAccount){
        this.username = username;
        this.password = password;
        this.createAccount = createAccount;
    }

    @Override
    public String getString() {
        return getString(tag);
    }

}
