package battleship.client.network;

import battleship.client.Log;
import battleship.client.network.response.ReceiveHandler;

import java.io.IOException;

public class Receiver {
    public static final String tag = "RECEIVER";
    private Thread thread;
    private static Receiver receiver;
    public static Receiver getInstance() {
        return receiver;
    }
    public Receiver(){
        receiver = this;
        thread = new Thread(this::run);
        thread.start();
    }

    public void interrupt(){
        thread.interrupt();
    }

    void run(){
        while (true){
            if(AppSocket.getInstance().getIsConnected()){
                try {
                    String input = AppSocket.getInstance().receive();
                    Log.log(tag, "received: "+input);
                    new ReceiveHandler(input);
                } catch (IOException e) {
                    Log.log(tag, "exception :"+e.getMessage());
                }
            }
            else {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {}
            }
            if(Thread.interrupted()){
                return;
            }
        }
    }

}
