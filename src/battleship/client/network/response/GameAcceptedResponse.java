package battleship.client.network.response;

import battleship.client.model.GameData;

public class GameAcceptedResponse implements Response {
    public static final String tag = "GAME_ACCEPTED";

    String opponentUsername;


    @Override
    public void run() {
        new GameData(opponentUsername);
        //todo makee UI start
    }
}
