package battleship.client.network.response;

import battleship.client.Controller.PageController;
import battleship.client.model.Data;

public class ConnectedResponse implements Response {
    public static final String tag = "CONNECTED";
    String username;
    int ranking;

    @Override
    public void run() {
        Data.getInstance().setUsername(username).setRanking(ranking);
        PageController.getInstance().setConnected();
    }
}
