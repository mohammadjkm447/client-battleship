package battleship.client.network.response;

import battleship.client.Controller.PageController;

public class GameEndedResponse implements Response {
    public static final String tag = "GAME_ENDED";

    boolean meWon;
    int myScore;
    int opponentScore;

    @Override
    public void run() {
        //todo end the game
    }
}
