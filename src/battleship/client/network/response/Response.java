package battleship.client.network.response;

public interface Response {
    public default void process(){
        new Thread(this::run).start();
    }
    void run();
}
