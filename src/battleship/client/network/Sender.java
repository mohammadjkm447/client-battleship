package battleship.client.network;

import battleship.client.Log;

import java.io.IOException;
import java.nio.ByteBuffer;

public class Sender {
    public static final String tag = "SENDER";
    private static final int timeout = 100;
    public static void sent(String str){
        ByteBuffer buffer = ByteBuffer.allocate(4+str.length());
        buffer.putInt(str.length());
        buffer.put(str.getBytes());
        while (true){
            if(!AppSocket.getInstance().getIsConnected()){
                try {
                    Thread.sleep(timeout);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            break;
        }
        try {
            AppSocket.getInstance().send(buffer);
            Log.log(tag, "sent: "+str);
        } catch (IOException e) {
            e.printStackTrace();
            Log.log(tag, "cannot send: "+str);
        }
    }
}
