package battleship.client.network;

public class ConnectionChecker{
    int timeout;
    int sentHiNum;
    int receiveHiNum;

    public ConnectionChecker(){
        timeout = 1000;
        sentHiNum = 1;
        receiveHiNum = 0;
        new Thread(this::run).start();
    }

    private void sentHi(){
        //todo sent hi to server
    }

    public void run() {
        try {
            while (true) {
                sentHi();
                Thread.sleep(timeout);
                if(sentHiNum != receiveHiNum){
                    AppSocket.getInstance().setIsConnected(false);
                    break;
                }
                sentHiNum++;
            }
        }catch (InterruptedException e) {
            System.out.println("Connection checker cannot sleep");
        }
    }
}



