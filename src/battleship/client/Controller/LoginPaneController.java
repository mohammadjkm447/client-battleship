package battleship.client.Controller;

import battleship.client.network.AppSocket;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class LoginPaneController implements ErrorControl{

    @FXML
    private TextField userIn;

    @FXML
    private TextField passIn;

    @FXML private Label errorLabel;

    @FXML
    private Button signBtn;

    @FXML
    void sign(ActionEvent e){
        String username = userIn.getText();
        String password = passIn.getText();
//        if(username.equals("") || password.equals("")){
//            setError("not correct inputs");
//            return;
//        }
        deleteError();
        new AppSocket(username, password, false);
        PageController.getInstance().setConnecting();
    }

    @Override
    public void setError(String str) {
        errorLabel.setText(str);
    }

    @Override
    public void deleteError() {
        errorLabel.setText("");
    }
}
