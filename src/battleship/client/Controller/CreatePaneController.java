package battleship.client.Controller;

import battleship.client.network.AppSocket;
import battleship.client.network.Request.FirstConnectionRequest;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CreatePaneController implements ErrorControl{

    @FXML
    private TextField uIn;

    @FXML
    private TextField pIn;

    @FXML
    private TextField prIn;

    @FXML
    private Button createBtn;

    @FXML
    private Label errorLabel;

    @FXML
    void create(ActionEvent e){
        String username = uIn.getText();
        String password = pIn.getText();
        String rPass = prIn.getText();
        if(!password.equals(rPass)){
            setError("Password is not the same");
            return;
        }
        new AppSocket(username, password, true);
        PageController.getInstance().setConnecting();
    }


    @Override
    public void setError(String str) {
        errorLabel.setText(str);
    }

    @Override
    public void deleteError() {
        errorLabel.setText("");
    }
}
