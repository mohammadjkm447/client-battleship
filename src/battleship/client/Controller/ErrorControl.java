package battleship.client.Controller;

public interface ErrorControl {
    void setError(String str);
    void deleteError();
}
