package battleship.client.Controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StartPageController implements Initializable {

    FXMLLoader loginLoader;
    FXMLLoader createLoader;

    @FXML
    private Button createBtn;

    @FXML
    private Button loginBtn;

    @FXML
    BorderPane root;

    @FXML
    void login(ActionEvent e) throws IOException {
        if(loginLoader == null) {
            loginLoader = new FXMLLoader(getClass().getResource("../View/LoginPane.fxml"));
            loginLoader.load();
        }
        root.setCenter(loginLoader.getRoot());
        loginBtn.setStyle("-fx-background-color:  #3c8313");
        createBtn.setStyle("-fx-background-color: #dd0b03");
        PageController.getInstance().setLoginController(loginLoader.getController());
    }
    @FXML
    void create(ActionEvent e) throws IOException {
        if(createLoader == null) {
            createLoader = new FXMLLoader(getClass().getResource("../View/createPane.fxml"));
            createLoader.load();
        }
        root.setCenter(createLoader.getRoot());
        loginBtn.setStyle("-fx-background-color: #dd0b03");
        createBtn.setStyle("-fx-background-color: #3c8313");
        PageController.getInstance().setCreateController(createLoader.getController());
    }

    public void setEnable(boolean enable){
        root.setDisable(!enable);
    }

    public void close(){
        root.getScene().getWindow().hide();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        PageController.getInstance().setStartPageController(this);
    }
}
