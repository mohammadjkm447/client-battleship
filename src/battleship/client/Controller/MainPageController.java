package battleship.client.Controller;

import battleship.client.model.Data;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.net.URL;
import java.util.ResourceBundle;


public class MainPageController implements Initializable {
    @FXML
    private Label statusLabel;

    @FXML
    private Label nameLabel;

    @FXML
    private Label rankingLabel;

    @FXML
    void close(ActionEvent event) {
        //todo close socket
        Platform.exit();
    }

    @FXML
    void newGame(ActionEvent event) {
        //todo start new game
    }

    @FXML
    void openSetting(MouseEvent event) {
        //todo open setting
    }

    public void setDisable(String why){
        //todo find a way make stage waiting
        statusLabel.setText(why);
        statusLabel.getScene().getRoot().setDisable(true);
    }

    public Stage getStage(){
        return (Stage)nameLabel.getScene().getWindow();
    }

    public void setEnable(){
        //todo enable waiting stage
        statusLabel.getScene().getRoot().setDisable(false);
        statusLabel.setText("");
    }

    public void updateScreen(){
        initUserProfile();
    }

    void initUserProfile(){
        nameLabel.setText(Data.getInstance().getUsername());
        rankingLabel.setText(String.valueOf(Data.getInstance().getRanking()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initUserProfile();
    }
}
