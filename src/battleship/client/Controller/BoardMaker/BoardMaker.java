package battleship.client.Controller.BoardMaker;

import battleship.client.model.GameModel.Cell;
import battleship.client.model.GameModel.GameBord;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class BoardMaker {
    GameBord gameBord ;

    public BoardMaker(VBox mybord, VBox enemybord) {
        Clear(mybord,enemybord);
        Cell[][] cells=getRandomField(false);
        Cell[][] cells2=getRandomField(true);
        for(int i=0 ; i<10 ; i++){
            HBox MyhBox = new HBox();
            HBox EnhBox = new HBox();
            for(int j=0 ;j<10 ;j++){
                MyhBox.getChildren().add(cells[i][j]);
                EnhBox.getChildren().add(cells2[i][j]);
            }
//            set size of the button correct
            mybord.getChildren().add(MyhBox);
            enemybord.getChildren().add(EnhBox);
        }
    }

    private void Clear(VBox mybord,VBox enemybord){
        mybord.getChildren().clear();
        enemybord.getChildren().clear();
    }


    private Cell[][] getRandomField(boolean isEnemy){

        gameBord =new GameBord(isEnemy);
        Cell[][] cells =gameBord.getBord();
        int ship4=1,ship3=2,ship2=3,ship1=4;

        while (true){
            int x=(int)(Math.random()*10);
            int y=(int)(Math.random()*10);

            if(ship4>0){
                if(SetShip(cells,x,y,4)){
                    ship4--;
                }
            }else if(ship3>0){
                if(SetShip(cells,x,y,3)){
                    ship3--;
                }
            }else if(ship2>0){
                if(SetShip(cells,x,y,2)){
                    ship2--;
                }
            }else if(ship1>0){
                if(SetShip(cells,x,y,1)){
                    ship1--;
                }
            }else {
                break;
            }

        }

        return cells;
    }


    private boolean SetShip(Cell[][] cells , int x , int y, int ShipLen){
        return CheckUp(cells,x,y,ShipLen) || CheckRight(cells,x,y,ShipLen) ||
                CheckDown(cells,x,y,ShipLen) || CheckLeft(cells,x,y,ShipLen);
    }


    private boolean CheckRectangle(Cell[][] cells ,int xStart,int xEnd,int yStart, int yEnd ,int ShipLen, boolean isShipPossible){
        for(int i=xStart ; i<=xEnd ; i++){
            for(int j=yStart ; j<=yEnd ; j++){
                try {
                    if(!isShipPossible){
                        if(isCoreOfRectangle(xStart,xEnd,yStart,yEnd,i,j) && !cells[i][j].isEditable()){
                            return false;
                        }
                    }else {
                        cells[i][j].setEditable(false);
                        if(isCoreOfRectangle(xStart,xEnd,yStart,yEnd,i,j)){
                            cells[i][j].setScore(getPoint(ShipLen));
                        }
                    }

                }catch (Exception ignore){
                    if(isCoreOfRectangle(xStart,xEnd,yStart,yEnd,i,j)){
                        return false;
                    }
                }

            }
        }
        return true;
    }

    private boolean isCoreOfRectangle(int xStart,int xEnd,int yStart, int yEnd,int i , int j){
       return i!=xStart && i!=xEnd && j!=yStart && j!=yEnd;
    }


    private boolean CheckUp(Cell[][] cells ,int x,int y,int ShipLen){
        if(CheckRectangle(cells,x-1,x+1,y-ShipLen,y+1,ShipLen,false)){
            return CheckRectangle(cells,x-1,x+1,y-ShipLen,y+1,ShipLen,true);
        }else {
            return false;
        }
    }
    private boolean CheckRight(Cell[][] cells ,int x,int y,int ShipLen){
        if(CheckRectangle(cells,x-1,x+ShipLen,y-1,y+1,ShipLen,false)){
            return CheckRectangle(cells,x-1,x+ShipLen,y-1,y+1,ShipLen,true);
        }else {
            return false;
        }
    }
    private boolean CheckLeft(Cell[][] cells ,int x,int y,int ShipLen){
        if(CheckRectangle(cells,x-ShipLen,x+1,y-1,y+1,ShipLen,false)){
            return CheckRectangle(cells,x-ShipLen,x+1,y-1,y+1,ShipLen,true);
        }else {
            return false;
        }
    }
    private boolean CheckDown(Cell[][] cells ,int x,int y,int ShipLen){
        if(CheckRectangle(cells,x-1,x+1,y-1,y+ShipLen,ShipLen,false)){
            return CheckRectangle(cells,x-1,x+1,y-1,y+ShipLen,ShipLen,true);
        }else {
            return false;
        }
    }



    private int getPoint(int len){
        if(len==1){
            return 4;
        }else if(len==2){
            return 3;
        }else if(len==3){
            return 2;
        }else {
            return 1;
        }

    }
}
