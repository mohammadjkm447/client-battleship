package battleship.client.Controller;

import battleship.client.Controller.BoardMaker.BoardMaker;
import battleship.client.model.GameData;
import battleship.client.model.Data;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;


import java.net.URL;
import java.util.ResourceBundle;

public class GamePageController implements Initializable {
    private static GamePageController gamePageController;


    public static GamePageController getInstance() {
        return gamePageController;
    }

    private boolean myTurn;

    @FXML private Label turnLabel;

    @FXML private Label myScoreLabel;

    @FXML private Label myUsername;

    @FXML private Label oppoUsername;

    @FXML private Label oppoScoreLabel;

    @FXML private VBox mybord;

    @FXML private VBox enemybord;

    @FXML private Button RandomBT;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        BoardMaker boardShipSeter =new BoardMaker(mybord,enemybord);

        RandomBT.setOnAction(e-> new BoardMaker(mybord,enemybord));
//        ********************************


        gamePageController = this;
        setScore(0, 0);
        myUsername.setText(Data.getInstance().getUsername());

        oppoUsername.setText(GameData.getInstance().getOpponentUsername());
        updateScore();

    }

    @FXML
    void randomBT(ActionEvent event) {
//        set ship random on bord
    }





    public void initGame(){
        //todo
    }

    @FXML
    void quit(ActionEvent event) {
        //todo quit app
    }

    public void setScore(int myScore, int oppnentScore){
        myScoreLabel.setText(String.valueOf(myScore));
        oppoScoreLabel.setText(String.valueOf(oppnentScore));
    }

    public void setFired(int x, int y, boolean isShip){
        // todo fire
    }

    public void setTurn(boolean myTurn){
        if(myTurn){
            turnLabel.setText("YOUR TURN");
            turnLabel.setStyle("-fx-text-fill: #00b31e");
            //todo enable Opponent map
        }
        else{
            turnLabel.setText("OPPONENT TURN");
            turnLabel.setStyle("-fx-text-fill: #d93e2f");
            //todo disable Opponent map
        }
    }


    void updateScore(){
        oppoScoreLabel.setText(String.valueOf(GameData.getInstance().getOpponentScore()));
        myScoreLabel.setText(String.valueOf(GameData.getInstance().getMyScore()));
    }
    public void switchTurn(){
        myTurn = !myTurn;
        setTurn(myTurn);
    }


}
