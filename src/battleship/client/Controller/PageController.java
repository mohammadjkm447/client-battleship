package battleship.client.Controller;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class PageController {
    private static PageController pageController;
    public static PageController getInstance() {
        if(pageController == null)
            pageController = new PageController();
        return pageController;
    }

    StartPageController startPageController;
    LoginPaneController loginPane;
    CreatePaneController createPane;
    MainPageController mainPageController;
    GamePageController gamePageController;

    private PageController(){
        pageController = this;
    }

    public void setStartPageController(StartPageController startPageController){
        this.startPageController = startPageController;
    }
    public void setLoginController(LoginPaneController loginController){
        loginPane = loginController;
        createPane = null;
    }
    public void setCreateController(CreatePaneController createController){
        createPane = createController;
        loginPane = null;
    }
    public void setMainPageController(MainPageController mainPageController){
        this.mainPageController = mainPageController;
        createPane = null;
        loginPane = null;
        startPageController = null;
    }
    public void setGamePageController(GamePageController gamePageController){
        this.gamePageController = gamePageController;
    }

    public void setDisconnected(){
        Platform.runLater(() -> {
            //todo manage page for disconnection
        });
    }
    public void setConnecting(){
        startPageController.setEnable(false);
    }

    public void setCreateError(String error){
        Platform.runLater(() -> {
            createPane.setError(error);
            startPageController.setEnable(true);
        });
    }
    public void setLoginError(String error){
        Platform.runLater(() -> {
            loginPane.setError(error);
            startPageController.setEnable(true);
        });
    }
    public void setConnected(){
        Platform.runLater(() -> {
            startPageController.close();
            openMainPage();
        });
    }

    //todo make setConnected and other connections method check the Page

    public void openMainPage(){
        FXMLLoader mainLoader = new FXMLLoader(getClass().getResource("../View/MainPage.fxml"));
        try {
            mainLoader.load();
            Stage stage = new Stage();
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.setScene(new Scene(mainLoader.getRoot()));
            MouseController.handle(mainLoader.getRoot(), stage);
            setMainPageController(mainLoader.getController());
            stage.show();
        } catch (IOException e) {
            System.out.println("cannot load main page:");
            System.out.println("    "+e.getMessage());
            Platform.exit();
        }
    }

    public void openGamePage(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../View/GamePage.fxml"));
        try {
            loader.load();
            Stage stage = new Stage();
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.initOwner(mainPageController.getStage());
            stage.setScene(new Scene(loader.getRoot()));
            MouseController.handle(loader.getRoot(), stage);
            setGamePageController(loader.getController());
            mainPageController.setDisable("IN GAME");
            stage.show();
        } catch (IOException e) {
            System.out.println("cannot load game page");
            System.out.println("    "+e.getMessage());
            Platform.exit();
        }
    }
}
