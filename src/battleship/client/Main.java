package battleship.client;

import battleship.client.model.GameData;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("View/StartPage.fxml"));
        primaryStage.setTitle("battleship");
        new GameData("example");
        loader.load();
        Scene s = new Scene(loader.getRoot());
        primaryStage.setScene(s);

        primaryStage.show();
    }

    public static void main(String[] args) {

        launch(args);
    }
}
