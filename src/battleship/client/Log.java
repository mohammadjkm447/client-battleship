package battleship.client;

public class Log {
    public static void log(String tag, String str) {
        System.out.println("***********************");
        System.out.println(tag);
        System.out.println(str);
        System.out.println("***********************");
    }
}
