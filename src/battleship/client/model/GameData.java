package battleship.client.model;

public class GameData {
    private static GameData gameData;
    public static GameData getInstance() {
        return gameData;
    }
    public static void removeInstance(){
        gameData = null;
    }
    private String opponentUsername;
    private int opponentScore;
    private int myScore;

    public GameData(String opponentUsername) {
        this.opponentUsername = opponentUsername;
        this.opponentScore = 0;
        this.myScore = 0;
        gameData = this;
    }

    public String getOpponentUsername() {
        return opponentUsername;
    }

    public void setOpponentUsername(String opponentUsername) {
        this.opponentUsername = opponentUsername;
    }

    public int getOpponentScore() {
        return opponentScore;
    }

    public void setOpponentScore(int opponentScore) {
        this.opponentScore = opponentScore;
    }

    public int getMyScore() {
        return myScore;
    }

    public void setMyScore(int myScore) {
        this.myScore = myScore;
    }
}
