package battleship.client.model.GameModel;

import javafx.scene.Node;

import java.util.ArrayList;

public class GameBord {
    private Cell[][] bord;
    private ArrayList<ShipType> shipTypes;
    private boolean enemyBord;


    public GameBord(boolean enemyBord) {
//        make new bord and fill it with cells
       bord = new Cell[10][10];
       for(int i=0 ;i<10 ;i++){
           for(int j=0 ;j<10;j++){
               bord[i][j]=new Cell(i,j,enemyBord);
           }
       }

//       set ship type
        shipTypes=new ArrayList<>();
        shipTypes.add(ShipType.Battleship);
        shipTypes.add(ShipType.Cruiser);
        shipTypes.add(ShipType.Cruiser);
        shipTypes.add(ShipType.Destroyer);
        shipTypes.add(ShipType.Destroyer);
        shipTypes.add(ShipType.Destroyer);
        shipTypes.add(ShipType.Frigate);
        shipTypes.add(ShipType.Frigate);
        shipTypes.add(ShipType.Frigate);
        shipTypes.add(ShipType.Frigate);




    }

    public Cell[][] getBord() {
        return bord;
    }

    public void setBord(Cell[][] bord) {
        this.bord = bord;
    }

    public ArrayList<ShipType> getShipTypes() {
        return shipTypes;
    }

    public void setShipTypes(ArrayList<ShipType> shipTypes) {
        this.shipTypes = shipTypes;
    }

    public boolean isEnemyBord() {
        return enemyBord;
    }

    public void setEnemyBord(boolean enemyBord) {
        this.enemyBord = enemyBord;
    }
}
