package battleship.client.model.GameModel;

import javafx.scene.control.Button;
import javafx.scene.layout.Background;

public class Cell extends Button {

    private int xPosition;
    private int yPosition;
    private int score =0;
    private boolean isEnemyCell;
    private boolean isSelected;
    private boolean isEditable = true;

    public Cell(int xPosition, int yPosition,boolean isEnemyCell) {
        super(" ");
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.isEnemyCell=isEnemyCell;
//        Button size
        setPrefHeight(500);
        setPrefWidth(500);
    }



    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
        if(! isEnemyCell){
            setStyle("-fx-background-color: gray");
        }
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }
}
