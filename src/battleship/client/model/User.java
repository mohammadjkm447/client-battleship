package battleship.client.model;

public class User {
    private String username;
    private int ranking;

    public User(String username, int ranking) {
        this.username = username;
        this.ranking = ranking;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }
}
