package battleship.client.model;

public class Data {
    static Data data;
    public static Data getInstance() {
        if (data == null)
            data = new Data();
        return data;
    }

    private Data(){

    }

    User user = new User("", 0);

    public String getUsername(){
        return user.getUsername();
    }
    public Data setUsername(String username){
        user.setUsername(username);
        return this;
    }
    public int getRanking(){
        return user.getRanking();
    }
    public Data setRanking(int ranking){
        user.setRanking(ranking);
        return this;
    }



}
